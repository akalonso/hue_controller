import requests
import json

# Reemplaza 'direccion_ip_puente' con la dirección IP de tu puente Hue
puente_ip = "172.31.4.1"
url = f"http://{puente_ip}/api"
data = {"devicetype": "mi_aplicacion#dispositivo_usuario"}

response = requests.post(url, json=data)
respuesta_json = response.json()

print(json.dumps(respuesta_json, indent=4))
