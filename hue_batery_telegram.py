import requests
import configparser
import os

# Leemos el archivo de configuración en la misma ruta del script
script_dir = os.path.dirname(os.path.realpath(__file__))
config_path = os.path.join(script_dir, 'config.ini')
config = configparser.ConfigParser()

# Comprobamos que el archivo de configuración y las secciones existan
if not config.read(config_path):
    raise FileNotFoundError(f"No se pudo leer el archivo de configuración: {config_path}")
if 'Philips' not in config:
    raise KeyError("La sección 'Philips' no se encuentra en el archivo de configuración")

# Extraer la configuración de Philips Hue
BRIDGE_IP = config['Philips']['BRIDGE_IP']
USER = config['Philips']['USER']
UMBRAL_BATERIA = int(config['Philips'].get('UMBRAL_BATERIA', 20))  # Valor por defecto de 20 si no se especifica

# Extraer la configuracion de telegram
BOT_TOKEN = config['Telegram']['bot_token']
CHAT_ID = config['Telegram']['chat_id']


# Funcion para leer ids excluidos
def leer_ids_excluidos():
    exclude_path = os.path.join(script_dir, 'exclude.txt')
    try:
        with open(exclude_path, 'r') as file:
            ids_excluidos = [line.strip() for line in file.readlines()]
        return ids_excluidos
    except FileNotFoundError:
        print(f"No se pudo leer el archivo de exclusión: {exclude_path}")
        return []

#Funcion enviar mensaje por telegram
def enviar_mensaje_telegram(mensaje):
    """Envía un mensaje a través de Telegram."""
    url = f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage"
    data = {
        'chat_id': CHAT_ID,
        'text': mensaje,
        'parse_mode': 'Markdown'
    }
    response = requests.post(url, data=data)
    if response.status_code != 200:
        print("Error al enviar mensaje a través de Telegram")

# Función para obtener nivel de batería
def obtener_nivel_bateria_bajo():
    ids_excluidos = leer_ids_excluidos()  # Obtener IDs a excluir
    url = f"http://{BRIDGE_IP}/api/{USER}/sensors"
    response = requests.get(url)
    mensaje = "Dispositivos Philips HUE con batería baja:\n"
    
    if response.status_code == 200:
        sensores = response.json()

        if isinstance(sensores, dict):
            for id, sensor in sensores.items():
                if "battery" in sensor.get("config", {}):
                    uniqueid = sensor.get("uniqueid", "")
                    if uniqueid not in ids_excluidos and sensor["config"]["battery"] < UMBRAL_BATERIA:
                        nombre = sensor.get("name", "Nombre no disponible")
                        mensaje += f"{nombre}, {uniqueid}, Batería: {sensor['config']['battery']}%\n"       
        else:
            mensaje = "Error al obtener los sensores: No se pudo procesar la respuesta de la API."
    else:
        mensaje = "Error al conectarse al puente Hue."
    
    return mensaje



if __name__ == "__main__":
    dispositivos=obtener_nivel_bateria_bajo()
    print(dispositivos)
    enviar_mensaje_telegram(dispositivos)